--[[
Copyright (C) 2018 Zarklord

This file is part of No Container Blocking.

The source code of this program is shared under the RECEX
SHARED SOURCE LICENSE (version 1.0).
The source code is shared for referrence and academic purposes
with the hope that people can read and learn from it. This is not
Free and Open Source software, and code is not redistributable
without permission of the author. Read the RECEX SHARED
SOURCE LICENSE for details 
The source codes does not come with any warranty including
the implied warranty of merchandise. 
You should have received a copy of the RECEX SHARED SOURCE
LICENSE in the form of a LICENSE file in the root of the source
directory. If not, please refer to 
<https://raw.githubusercontent.com/Recex/Licenses/master/SharedSourceLicense/LICENSE.txt>
]]

if not SetupGemCoreEnv then
	return
end
SetupGemCoreEnv()

PrefabFiles = {
    "container_opener",
}

GLOBAL.setfenv(1, GLOBAL)

local Container_Replica = require("components/container_replica")

local _SetOpener = Container_Replica.SetOpener

local _OnRemoveFromEntity = Container_Replica.OnRemoveFromEntity
function Container_Replica:OnRemoveFromEntity()
    _OnRemoveFromEntity(self)
    if self.opener and not TheWorld.ismastersim then
        self.opener._parent = nil
        self.inst:RemoveEventCallback("onremove", self.ondetachopener, self.opener)
        self:DetachOpener()
    elseif TheWorld.ismastersim then
        for player, opener in pairs(self.openers) do
            opener:Remove()
            self.openers[player] = nil
        end
    end
end

Container_Replica.OnRemoveEntity = Container_Replica.OnRemoveFromEntity

function Container_Replica:AttachClassified(classified)
    self.classified = classified

    self.ondetachclassified = function() self:DetachClassified() end
    self.inst:ListenForEvent("onremove", self.ondetachclassified, classified)

    classified:InitializeSlots(self:GetNumSlots())
end

function Container_Replica:DetachClassified()
    self.inst:RemoveEventCallback("onremove", self.ondetachclassified, self.classified)
    self.classified = nil
    self.ondetachclassified = nil
end

local function OnRefreshCrafting(inst)
    if ThePlayer ~= nil and ThePlayer.HUD ~= nil then
        ThePlayer:PushEvent("refreshcrafting")
    end
end

local function OpenContainer(inst, self, snap)
    self.opentask = nil

    --V2C: don't animate to and from the backpack position 
    --     when re-opening inventory as Werebeaver->Woodie
    local inv = snap and ThePlayer ~= nil and ThePlayer.HUD ~= nil and ThePlayer.HUD.controls.inv or nil
    snap = inv ~= nil and not inv.rebuild_pending

    self:Open(ThePlayer)
    OnRefreshCrafting(inst)

    if snap and inv.rebuild_pending then
        inv.rebuild_snapping = true
    end
end

function Container_Replica:AttachOpener(opener)
    self.opener = opener

    self.ondetachopener = function() self:DetachOpener() end
    self.inst:ListenForEvent("onremove", self.ondetachopener, opener)

    local inv = self.issidewidget and ThePlayer ~= nil and ThePlayer.HUD ~= nil and ThePlayer.HUD.controls.inv or nil
    self.opentask = self.inst:DoTaskInTime(0, OpenContainer, self, inv ~= nil and (not inv.shown or inv.rebuild_snapping))

    if self.issidewidget then
        self.inst:ListenForEvent("itemget", OnRefreshCrafting)
        self.inst:ListenForEvent("itemlose", OnRefreshCrafting)
    end
end

function Container_Replica:DetachOpener()
    self.inst:RemoveEventCallback("onremove", self.ondetachopener, self.opener)
    self.opener = nil
    self.ondetachopener = nil
    if self.issidewidget then
        self.inst:RemoveEventCallback("itemget", OnRefreshCrafting)
        self.inst:RemoveEventCallback("itemlose", OnRefreshCrafting)
        OnRefreshCrafting(self.inst)
    end
    self:Close()
end

function Container_Replica:AddOpener(opener)
    local opencount = self.inst.components.container.opencount
    if opencount == 1 then
        _SetOpener(self, opener)
    elseif opencount > 1 then
        self.classified.Network:SetClassifiedTarget(nil)
        if self.inst.components.container ~= nil then
            for k, v in pairs(self.inst.components.container.slots) do
                v.replica.inventoryitem:SetOwner(self.inst)
            end
        end
    end
    self.openers[opener] = self.inst:SpawnChild("container_opener")
    self.openers[opener].Network:SetClassifiedTarget(opener)
end

function Container_Replica:RemoveOpener(opener)
    local opencount = self.inst.components.container.opencount
    if opencount == 0 then
        _SetOpener(self, nil)
    elseif opencount == 1 then
        _SetOpener(self, table.getkeys(self.inst.components.container.openlist)[1])
    elseif opencount > 1 then
        self.classified.Network:SetClassifiedTarget(nil)
        if self.inst.components.container ~= nil then
            for k, v in pairs(self.inst.components.container.slots) do
                v.replica.inventoryitem:SetOwner(self.inst)
            end
        end
    end
    if self.openers[opener] then
        self.openers[opener]:Remove()
        self.openers[opener] = nil
    end
end

for k, v in pairs({"IsOpenedBy", "IsHolding", "IsEmpty", "IsFull"}) do
    local _OLD = Container_Replica[v]
    Container_Replica[v] = function(self, ...)
        if self.inst.components.container ~= nil then
            return _OLD(self, ...)
        else
            return self.opener ~= nil and _OLD(self, ...)
        end
    end
end

local _GetItemInSlot = Container_Replica.GetItemInSlot
function Container_Replica:GetItemInSlot(...)
    if self.inst.components.container ~= nil then
        return _GetItemInSlot(self, ...)
    else
        return self.opener ~= nil and _GetItemInSlot(self, ...) or nil
    end
end

local _GetItems = Container_Replica.GetItems
function Container_Replica:GetItems(...)
    if self.inst.components.container ~= nil then
        return _GetItems(self, ...)
    else
        return self.opener ~= nil and _GetItems(self, ...) or {}
    end
end

local _Has = Container_Replica.Has
function Container_Replica:Has(prefab, amount, ...)
    if self.inst.components.container ~= nil then
        return _Has(self, prefab, amount, ...)
    elseif self.classified ~= nil and self.opener ~= nil then
        return _Has(self, prefab, amount, ...)
    else
        return amount <= 0, 0
    end
end

local _IsBusy = Container_Replica.IsBusy
function Container_Replica:IsBusy(...)
    return _IsBusy(self, ...) and self.opener ~= nil
end

GEMENV.AddClassPostConstruct("components/container_replica", function(self)
    self.container_opener = nil
    self.openers = {}
    self.opener = nil

    if TheWorld.ismastersim then
        self.SetOpener = function() end
    else
        if self.opener == nil and self.inst.container_opener ~= nil then
            self.opener = self.inst.container_opener
            self.inst.container_opener.OnRemoveEntity = nil
            self.inst.container_opener = nil
            self:AttachOpener(self.opener)
        end
    end
end)

GEMENV.AddPrefabPostInit("container_classified", function(inst)
    if not TheWorld.ismastersim then
        UpvalueHacker.SetUpvalue(inst.ReturnActiveItemToSlot, 0.5, "PushItemGet", "TIMEOUT")
    end
end)

if not TheNet:GetIsServer() then return end

local RPC_HANDLERS = UpvalueHacker.GetUpvalue(SendRPCToServer, "RPC_HANDLERS")

for i, v in ipairs({"PutOneOfActiveItemInSlot", "PutAllOfActiveItemInSlot", "TakeActiveItemFromHalfOfSlot", "TakeActiveItemFromAllOfSlot",
                   "AddOneOfActiveItemToSlot", "AddAllOfActiveItemToSlot", "SwapActiveItemWithSlot", "MoveItemFromAllOfSlot", "MoveItemFromHalfOfSlot"}) do
    local _func = RPC_HANDLERS[RPC[v]]
    RPC_HANDLERS[RPC[v]] = function(player, slot, container, destcontainer, ...)
        local srccontainer = container ~= nil and container.components.container or nil
        local dstcontainer = destcontainer ~= nil and destcontainer.components.container or nil
        local _funcsrc, _funcdest
        if srccontainer ~= nil then
            _funcsrc = srccontainer[v]
            srccontainer[v] = function(self, ...)
                local args = {...}
                args[#args + 1] = player
                _funcsrc(self, unpack(args))
            end
        end
        if dstcontainer ~= nil then
            _funcdest = dstcontainer[v]
            dstcontainer[v] = function(self, ...)
                local args = {...}
                args[#args + 1] = player
                _funcdest(self, unpack(args))
            end
        end
        _func(player, slot, container, destcontainer, ...)
        if srccontainer ~= nil then
            srccontainer[v] = _funcsrc
        end
        if dstcontainer ~= nil then
            dstcontainer[v] = _funcdest
        end
    end
end

local _DoWidgetButtonAction = RPC_HANDLERS[RPC.DoWidgetButtonAction]
RPC_HANDLERS[RPC.DoWidgetButtonAction] = function(player, action, target, mod_name, ...)
    local _opener
    local playercontroller = player.components.playercontroller
    if playercontroller ~= nil and playercontroller:IsEnabled() and not player.sg:HasStateTag("busy") then
        local container = target ~= nil and target.components.container or nil
        if container == nil or container:IsOpenedBy(player) then
            _opener = rawget(container, "opener")
            container.opener = player
        end
    end
    _DoWidgetButtonAction(player, action, target, mod_name, ...)
    if playercontroller ~= nil and playercontroller:IsEnabled() and not player.sg:HasStateTag("busy") then
        local container = target ~= nil and target.components.container or nil
        if container == nil or container:IsOpenedBy(player) then
            container.opener = _opener
        end
    end
end

local Inventory = require("components/inventory")

local _CanAccessItem = Inventory.CanAccessItem
function Inventory:CanAccessItem(item, ...)
    local owner = item.components.inventoryitem.owner
    local _oldopener
    if owner ~= nil and owner.components.container ~= nil and owner.components.container:IsOpenedBy(self.inst) then
        _oldopener = rawget(owner.components.container, "opener")
        owner.components.container.opener = self.inst
    end
    local retvals = {_CanAccessItem(self, item, ...)}
    if owner ~= nil and owner.components.container ~= nil and owner.components.container:IsOpenedBy(self.inst) then
        owner.components.container.opener = _oldopener
    end
    return unpack(retvals)
end

local Eater = require("components/eater")

local _Eat = Eater.Eat
function Eater:Eat(food, feeder)
    local _opener
    if feeder ~= self.inst and self.inst.components.inventoryitem ~= nil then
        local owner = self.inst.components.inventoryitem:GetGrandOwner()
        if owner ~= nil and owner.components.container ~= nil and owner.components.container:IsOpenedBy(feeder) then
            _opener = rawget(owner.components.container, "opener")
            owner.components.container.opener = feeder
        end
    end
    local retvals = {_Eat(self, food, feeder)}
    if feeder ~= self.inst and self.inst.components.inventoryitem ~= nil then
        local owner = self.inst.components.inventoryitem:GetGrandOwner()
        if owner ~= nil and owner.components.container ~= nil and owner.components.container:IsOpenedBy(feeder) then
            owner.components.container.opener = _opener
        end
    end
    return unpack(retvals)
end

local Container = require("components/container")

function Container:Open(doer)
    if doer ~= nil and self.openlist[doer] == nil then
        self.inst:StartUpdatingComponent(self)

        local inventory = doer.components.inventory
        if inventory ~= nil then
            for k, v in pairs(inventory.opencontainers) do
                if k.prefab == self.inst.prefab or k.components.container.type == self.type then
                    k.components.container:Close(doer)
                end
            end

            inventory.opencontainers[self.inst] = true
        end

        self.openlist[doer] = true
        self.opencount = self.opencount + 1
        self.inst.replica.container:AddOpener(doer)

        if doer.HUD ~= nil then
            doer.HUD:OpenContainer(self.inst, self:IsSideWidget())
            if self:IsSideWidget() then
                TheFocalPoint.SoundEmitter:PlaySound("dontstarve/wilson/backpack_open")
            end
        elseif self.widget ~= nil
            and self.widget.buttoninfo ~= nil
            and doer.components.playeractionpicker ~= nil then
            doer.components.playeractionpicker:RegisterContainer(self.inst)
        end

        self.inst:PushEvent("onopen", {doer = doer})

        if self.onopenfn ~= nil and self.opencount == 1 then
            self.onopenfn(self.inst, {doer = doer})
        end

        if self.onanyopenfn ~= nil then
            self.onanyopenfn(self.inst, {doer = doer})
        end
    end
end

function Container:Close(doer)
    if doer == nil then
        for doer, _ in pairs(self.openlist) do
            self:Close(doer)
        end
        return
    end
    if doer ~= nil and self.openlist[doer] ~= nil then
        self.openlist[doer] = nil
        self.opencount = self.opencount - 1
        self.inst.replica.container:RemoveOpener(doer)

        if self.opencount == 0 then
            self.inst:StopUpdatingComponent(self)
        end

        if doer.HUD ~= nil then
            doer.HUD:CloseContainer(self.inst, self:IsSideWidget())
            if self:IsSideWidget() then
                TheFocalPoint.SoundEmitter:PlaySound("dontstarve/wilson/backpack_close")
            end
        elseif doer.components.playeractionpicker ~= nil then
            doer.components.playeractionpicker:UnregisterContainer(self.inst)
        end

        if doer.components.inventory ~= nil then
            doer.components.inventory.opencontainers[self.inst] = nil
        end

        if self.onclosefn ~= nil and self.opencount == 0 then
            self.onclosefn(self.inst, {doer = doer})
        end

        if self.onanyclosefn ~= nil then
            self.onanyclosefn(self.inst, {doer = doer})
        end

        self.inst:PushEvent("onclose", {doer = doer})
    end
end

function Container:IsOpen()
    return self.opencount > 0
end

function Container:IsOpenedBy(guy)
    return self.openlist[guy]
end

function Container:OnUpdate(dt)
    if self.opencount == 0 then
        self.inst:StopUpdatingComponent(self)
    else
        for doer, _ in pairs(self.openlist) do
            if not (self.inst.components.inventoryitem ~= nil and
                    self.inst.components.inventoryitem:IsHeldBy(doer)) and
                    ((doer.components.rider ~= nil and doer.components.rider:IsRiding()) or
                    not (doer:IsNear(self.inst, 3) and
                    CanEntitySeeTarget(doer, self.inst))) then
                self:Close(doer)
            end
        end
    end
end

local function QueryActiveItem(self)
    local inventory = self.opener ~= nil and self.opener.components.inventory or nil
    return inventory, inventory ~= nil and inventory:GetActiveItem() or nil
end

local _PutAllOfActiveItemInSlot = Container.PutAllOfActiveItemInSlot
function Container:PutAllOfActiveItemInSlot(slot, ...)
    local inventory, active_item = QueryActiveItem(self)
    local item = self:GetItemInSlot(slot)
    if active_item ~= nil and item ~= nil then
        return self:SwapActiveItemWithSlot(slot, ...)
    end
    _PutAllOfActiveItemInSlot(self, slot, ...)
end

local _SwapActiveItemWithSlot = Container.SwapActiveItemWithSlot
function Container:SwapActiveItemWithSlot(slot, ...)
    local inventory, active_item = QueryActiveItem(self)
    local item = self:GetItemInSlot(slot)
    if active_item ~= nil and item == nil then
        return self:PutAllOfActiveItemInSlot(slot, ...)
    end
    _SwapActiveItemWithSlot(self, slot, ...)
end

for i, v in ipairs({"PutOneOfActiveItemInSlot", "PutAllOfActiveItemInSlot", "TakeActiveItemFromHalfOfSlot", "TakeActiveItemFromAllOfSlot",
                   "AddOneOfActiveItemToSlot", "AddAllOfActiveItemToSlot", "SwapActiveItemWithSlot", "MoveItemFromAllOfSlot", "MoveItemFromHalfOfSlot"}) do
    local _func = Container[v]
    Container[v] = function(self, ...)
        local exargs = {...}
        local opener = exargs[#exargs]
        local _opener
        if type(opener) == "table" and opener.GUID and opener:HasTag("player") then
            _opener = rawget(self, "opener")
            self.opener = opener
        end
        _func(self, ...)
        self.opener = _opener
    end
end

function Container:IsOpen()
    return false
end

Container.OnRemoveEntity = Container.Close
Container.OnRemoveFromEntity = Container.Close

local function ContainerPostInit(self)
    self.openlist = {}
    self.opencount = 0
    removesetter(self, "opener")
    local __metatable = debug.getmetatable(self)
    local __index = __metatable.__index
    function __metatable:__index(name)
        return name == "opener" and self:IsOpenedBy(TheLocalPlayer) and TheLocalPlayer or __index(self, name)
    end
end

GEMENV.AddComponentPostInit("container", ContainerPostInit)
GEMENV.AddComponentPostInit("boatcontainer", ContainerPostInit) --IA compatability

local PreventLimboList = {}

local _SetInLimbo = Entity.SetInLimbo
function Entity:SetInLimbo(bool, ...)
    local inst = Ents[self:GetGUID()]
    if PreventLimboList[inst] then
        return _SetInLimbo(self, false, ...)
    end
    return _SetInLimbo(self, bool, ...)
end

local function PreventLimbo(ent)
    ent.entity:SetInLimbo(false)
    PreventLimboList[ent] = true
end

local function FixLimbo(ent)
    PreventLimboList[ent] = nil
    if ent:IsInLimbo() then
        ent.entity:SetInLimbo(true)
    end
end

--while this is a replica, we only mess with a server side function.
local Inventoryitem_Replica = require("components/inventoryitem_replica")

function Inventoryitem_Replica:SetOwner(owner)
    local opencount = owner ~= nil and owner.components.container ~= nil and owner.components.container.opencount or 0
    if opencount > 1 then
        PreventLimbo(self.inst)
        if self.inst.Network ~= nil then
            self.inst.Network:SetClassifiedTarget(nil)
        end
        if self.classified ~= nil then
            self.classified.Network:SetClassifiedTarget(nil)
        end
    else
        owner = opencount == 0 and owner or opencount == 1 and table.getkeys(owner.components.container.openlist)[1] or nil
        FixLimbo(self.inst)
        if self.inst.Network ~= nil then
            self.inst.Network:SetClassifiedTarget(owner)
        end
        if self.classified ~= nil then
            self.classified.Network:SetClassifiedTarget(owner or self.inst)
        end
    end
end